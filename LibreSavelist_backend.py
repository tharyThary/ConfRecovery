import os
import os.path
import shutil
import json
import subprocess

#copy function
def copy_files(input_path,output_path):
    subprocess.Popen(["cp","-r",input_path,output_path])

def configParsing():
    pathToConfig = os.path.expanduser('~')+'/.config/libresavelist/config'
    with open(pathToConfig) as file:
        contentFile = file.read()
        config = json.loads(contentFile)
        return config
