#importing modules
from simple_term_menu import TerminalMenu
import config
import questionary
import os
import LibreSavelist_backend

lsl =  LibreSavelist_backend

#menu with confirm copy
def confirm_copy():
    dirs_number = len(os.listdir(config.input_path))
    ccopy = questionary.confirm("Copy " + str(dirs_number) + " directories and files?").ask()

    if ccopy == True:
        input = lsl.configParsing()['input_path']
        output = lsl.configParsing()['output_path']
        print("Input path: " + input)
        print("Output path: " + output)
        lsl.copy_files(input, output)
    if ccopy == False:
        main()


#main menu
def main():
    options = ["[c] Copy configs", "[e] Exit"]
    #
    main_menu = TerminalMenu(
        options,
        title="      Main Menu"
    )
    menu_entry_index = main_menu.show()
    select = [menu_entry_index]
    if select == [0]:
        confirm_copy()

main()
