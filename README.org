#+TITLE: LibreSaveList
#+AUTHOR: Thary (thary@riseup.net)

* About LibreSaveList
* Installation
* Configuration
Create configuration file in directory /home/$USER/.config/libresavelist/ with name "config" (/home/$USER/.config/libresavelist/config). Config file:
#+BEGIN_SRC json
{
		"input_path":"/home/user/.config",
		"output_path":"/home/user/Configs"
}
#+END_SRC
WARNING! Do not use  relative paths (for example "~/dir","$HOME/dir","/home/$USER/dir")
* License
This project is licensed under the GPL Version 3 License - see the [[/LICENSE][LICENSE]] file for details
